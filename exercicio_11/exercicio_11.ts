/*
Faça um programa que receba o numero positivo e maior que zero, calcule e mostre:
a)  O numero digitado ao quadrado.
b) O numero digitado ao cubo.
c) A raiz quadrada do numero digitado.
d) A rqiz cúbica do número digitado.
*/
//Inicio
namespace exercicio_11 {
  //Entrada de dados
  const numero = 10;

  let numQ: number;
  numQ = Math.pow(numero, 2);
  //numQ = numero ** numero;
  // numQ = numero * numero;

  let numC: number;

  numC = numero * numero * numero;
  //numC = Math.pow(numero, 3);
  //numC = numero** 3:
  let raizQ: number;

  raizQ = Math.sqrt(numero);

  let raizC: number;

  raizC = Math.cbrt(numero);

  console.log(
    ` O número elevado ao quadrado: ${numQ} \n O número elevado ao cubo: ${numC} \n A raiz quadrada do número: ${raizQ} \n A raiz cúbica do número: ${raizC} \n`
  );
}
